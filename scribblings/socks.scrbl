#lang scribble/manual

@title[#:style '(toc)]{Socks}
@author{Karrq}

@defmodule[socks]

@bold{This package is a library to interact with SOCKS proxies.}

What can you do with this library right now?

You can connect to any SOCKS5 proxy as a client with no authentication.

@table-of-contents[]

@section[#:tag "planned"]{Planned features}
@itemlist[@item{Server support: host your own socks proxy with this library}
          @item{Support for standard authentication methods: username&password, GSSAPI}
          @item{Support for all IANA authentication methods}
          @item{Support for all 3 commands: BIND, ASSOCIATE}
          @item{Support for older versions of the socks protocol: Socks4, Socks4a}]

@section[]{Notes}
This library is very much work in progress, expect unpolished api, missing documentations, bugs and bad practices!

@include-section["api.scrbl"]