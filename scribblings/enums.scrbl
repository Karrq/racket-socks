#lang scribble/manual
@require[(for-label socks)]

@title[#:tag "enums"]{Enums}
Two enums are used in the library, one to represent the set of authentication methods and the other one
to represet the set of commands that can be sent to the proxy server.

@section[#:tag "conn-cmd"]{conn-cmd}
This enum represents the possible commands sent to the proxy server.

@defproc[#:kind "predicate" (conn-cmd? [item (any/c)]) boolean?]{
 Enum selector.

 Returns @racket[#t] whenever the element passed is one of the variants listed below.}

@defthing[#:kind "conn-cmd" Connect conn-cmd?]{
 Command to establish a TCP stream binding.}

@section[#:tag "socks5-auth"]{socks-auth}
This enum represents the possible authentication methods defined by the protocol.
                                        