 #lang scribble/manual
@require[socks
         (for-label socks
                    
                    racket/base
                    racket/tcp

                    net/ip
                    net/dns)]

@title[#:tag "api"]{API}
@declare-exporting[socks/socks5]

@defproc[(proxify [target-addr string?]
                  [#:target-port target-port number? 80]
                  [#:proxy-addr proxy-addr string? "localhost"]
                  [#:proxy-port proxy-port number? 1080]
                  [#:socks-cmd cmd conn-cmd? Connect]
                  [#:tcp tcp-pair tcp-pair/c #f])
                  tcp-pair/c]{
 If a @racket[tcp-pair] is provided it's assumed to be an open connection to the proxy server.
 If no @racket[tcp-pair] is provided the function will use @racket[tcp-connect]
 to create a tcp connection with the proxy server.

 The procedure will then continue and handshake with the proxy server and request connection
 to the target using the specified @racket[socks-cmd].

 The return values are a pair of the input and output port of the connection.

 @racket[target-addr] can either be a domain or an ip address.

  The ip address format is the same as @racket[make-ip-address].

  The domain name @bold{must} be prepended by either "socks5://" or "socks5h://", according to SOCKSCURL
  convention. The first indicates that the domain name resolution happens before
  requesting the connection to the proxy, therefore done from the client machine
  (see @racket[dns-get-address] for specifics on how this resolution happens).
  If the domain is prepended by the latter ("socks5h://") then the responsability of resolving the
  domain is given to the proxy server.}

@defproc[#:kind "contract"
         (tcp-pair/c [pair? (any/c)]) boolean?]{
 Contract used to determine whenever the item passed is a @racket[tcp-pair].
 A @racket[tcp-pair] is a pair where the car is the @racket[input-port]
 and the cdr is the @racket[output-port] of a tcp connection.}

@include-section["enums.scrbl"]