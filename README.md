socks
======
This package is a library to interact with SOCKS proxies.

Very much work in progress, expect unpolished api, missing documentations, bugs and bad practices!

Currently only serves as socks5 client with no authentication

Licensed under either MIT or APACHE 2.0 at your choice
